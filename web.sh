#!/bin/bash

if [[ $EUID -ne 0 ]]; then
  echo "You must be a root user" 2>&1
  exit 1
fi

LOG=/tmp/web-report.log

echo "Starting analysis `date`" > $LOG
hostname >> $LOG
cat /etc/hosts >> $LOG
w >> $LOG
free -m >> $LOG
netstat -nlpt >> $LOG
df -Th >> $LOG
du -sh /var/www/ >> $LOG
curl -sL https://raw.githubusercontent.com/richardforth/apache2buddy/master/apache2buddy.pl | perl >> $LOG
echo "Finished analysis `date`" >> $LOG



#!/bin/bash

if [[ $EUID -ne 0 ]]; then
  echo "You must be a root user" 2>&1
  exit 1
fi

LOG=/tmp/db-report.log

echo "Starting analysis `date`" > $LOG
hostname >> $LOG
cat /etc/hosts >> $LOG
w >> $LOG
free -m >> $LOG
netstat -nlpt >> $LOG
df -Th >> $LOG
du -sh /var/lib/mysql >> $LOG
wget -q mysqltuner.pl --no-check-certificate -O mysqltuner.pl 
perl mysqltuner.pl  >> $LOG
echo "Finished analysis `date`" >> $LOG

